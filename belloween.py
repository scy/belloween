#!/usr/bin/env python3

from signal import pause
from threading import Thread
from time import monotonic, sleep

from gpiozero import Button, DigitalOutputDevice
import pygame


def log(*args):
    print(*args)


class Belloween:

    def __init__(self, bell_pin=11, strobe_pin=14, strobe_time=1.8, pause_time=30, soundfile="belloween.wav", wait_before_flash=0.2):
        self.bell = Button(bell_pin, bounce_time=0.05)
        self.strobe = DigitalOutputDevice(strobe_pin)
        self.strobe_time = strobe_time
        self.pause_time = pause_time
        self.wait_before_flash = wait_before_flash
        self.last_bell_press = None
        pygame.mixer.init()
        self.sound = pygame.mixer.Sound(soundfile)

    def bell_pressed(self):
        now = monotonic()
        diff = None if self.last_bell_press is None else now - self.last_bell_press
        if diff is not None and diff < self.pause_time:
            log(f"Ignoring bell press: Last one was {round(diff, 1)} seconds ago, but {self.pause_time} seconds pause are configured.")
            return
        self.last_bell_press = now
        log("Bell was pressed!")
        flash = Thread(target=self.flash)
        self.sound.play()
        if self.wait_before_flash:
            sleep(self.wait_before_flash)
        flash.start()
        flash.join()

    def flash(self):
        self.strobe.on()
        log("Strobe is on ...")
        sleep(self.strobe_time)
        self.strobe.off()
        log("Strobe is off.")

    def run(self):
        log("Running.")
        self.bell.when_pressed = self.bell_pressed
        pause()


if __name__ == "__main__":
    Belloween().run()
