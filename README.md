# “Belloween”

_Like, wenn your doorbell turns all halloweeny._

## What is it?

For Halloween 2021, I wired my doorbell to a Raspberry Pi so that when somebody rang, there would be “lightning” and thunder coming from inside the house (visible through my glass front door), and then there would be someone screaming.

## What do you need?

* A Raspberry Pi. I chose an old model 1B with 2 GB of SD memory (mainly required for the OS). It’s totally fast enough for this project.
* A relay that can be triggered by the wiring from your doorbell. I’m using a Finder 40.52.8.012.0000 12V AC(!) relay.
* Some flashy lights. I had some car strobe lights lying around, which is why I didn’t have to program any LED pattern or something, just trigger another relay on and off at the right time; the lights would strobe on their own.
* A pair of speakers (or a single one, really) for the sound effects.
* Some matching power supplies and jumper cables to wire everything together.

## How to wire it

Right now I don’t have time to do a full-on tutorial for beginners.
Maybe I’ll improve this documentation later.

Basically, to use the code exactly as it is, connect the doorbell input relay to pin 11 of the Pi, so that the relay is normally open and the pin is floating.
When the bell is triggered, the relay should close and connect pin 11 to ground.
The code will set up the pin with an internal pull-up.

Next, connect the strobe output relay to pin 14.
The code will set the pin low initially, and to high during the time the strobe should flash.

## How to set up the software

This project requires Python 3 and the `gpiozero` and `pygame` Python libraries.
`gpiozero` might already be installed on your Pi.

There’s a systemd unit included in the repo that you can use, or you simply run `belloween.py` manually.

You can configure some things, like timing and pin numbers, by supplying parameters to the `Belloween()` constructor.

## Development & Contribution

I don’t really intend to develop this project any further, I’ve published it to document it and maybe as an inspiration for others.

## License

The code is licensed under the 2-Clause BSD License.

The thunderclap & scream sound effects are based on:

* [thunder.mp3](https://freesound.org/people/DragishaRambo21/sounds/345920/) by DragishaRambo21, CC-BY-NC-3.0
* [Woman Scream 5](https://freesound.org/people/J.Zazvurek/sounds/353292/) by J.Zazvurek, CC-BY-3.0
